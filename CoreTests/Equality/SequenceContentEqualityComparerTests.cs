using Supernova.Foundation.Core.Equality;
using Xunit;

namespace Supernova.Foundation.CoreTests.Equality
{
    public class SequenceContentEqualityComparerTests
    {
        [Theory]
        [InlineData(null, null, true)]
        [InlineData(new int[0], null, false)]
        [InlineData(null, new int[0], false)]
        [InlineData(new int[0], new int[0], true)]
        [InlineData(new int[0], new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2, 4 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 3, 2 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2, 3 }, new[] { 1, 2, 3 }, true)]
        public void SequenceContentEqualityComparer_Equals_ReturnsEquality(int[] array1, int[] array2, bool expected)
        {
            // Act
            bool result = new SequenceContentEqualityComparer<int>().Equals(array1, array2);

            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(null, null, true)]
        [InlineData(new int[0], null, false)]
        [InlineData(null, new int[0], false)]
        [InlineData(new int[0], new int[0], true)]
        [InlineData(new int[0], new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2, 4 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 3, 2 }, new[] { 1, 2, 3 }, true)]
        [InlineData(new[] { 1, 2, 3 }, new[] { 1, 2, 3 }, true)]
        public void SequenceContentEqualityComparer_WithIgnoredOrder_Equals_ReturnsEquality(int[] array1, int[] array2, bool expected)
        {
            // Act
            bool result = new SequenceContentEqualityComparer<int>(true).Equals(array1, array2);

            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(null, null, true)]
        [InlineData(new int[0], null, false)]
        [InlineData(null, new int[0], false)]
        [InlineData(new int[0], new int[0], true)]
        [InlineData(new int[0], new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2, 4 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 3, 2 }, new[] { 1, 2, 3 }, false)]
        [InlineData(new[] { 1, 2, 3 }, new[] { 1, 2, 3 }, true)]
        public void SequenceContentEqualityComparer_GetHashCode_ReturnsSameHashCodeForEqualArguments(int[] array1, int[] array2, bool equal)
        {
            // Act
            int hash1 = new SequenceContentEqualityComparer<int>().GetHashCode(array1);
            int hash2 = new SequenceContentEqualityComparer<int>().GetHashCode(array2);

            // Assert
            Assert.Equal(equal, hash1 == hash2);
        }
    }
}
