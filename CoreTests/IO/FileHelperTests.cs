﻿using System;
using System.IO;
using System.Linq;
using Supernova.Foundation.Core.IO;
using Xunit;

namespace Supernova.Foundation.CoreTests.IO
{
    public class FileHelperTests : IDisposable
    {
        public FileHelperTests()
        {
            Directory.CreateDirectory("./foo");
            Directory.CreateDirectory("./bar");
            Directory.CreateDirectory("./baz/dir1/dir2/dir3");
            Directory.CreateDirectory("./baz/dir1/dir4");

            File.WriteAllText("./foo/ame.txt", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            File.WriteAllText("./foo/yuki.txt", "Duis sit amet dolor eget quam viverra elementum ac eget purus.");
            File.WriteAllText("./baz/dir1/file0.txt", "Foo Bar Baz");
            File.WriteAllText("./baz/dir1/dir2/file1.txt", "Foo Bar Baz");
            File.WriteAllText("./baz/dir1/dir2/file2.txt", "Foo Bar Baz");
            File.WriteAllText("./baz/dir1/dir2/dir3/file3.txt", "Foo Bar Baz");
            File.WriteAllText("./baz/dir1/dir2/dir3/file4.txt", "Foo Bar Baz");
            File.WriteAllText("./baz/dir1/dir4/file5.txt", "Foo Bar Baz");
        }

        [Fact]
        public void FileHelper_MoveToParentDirectory_WithInvalidArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => FileHelper.MoveToParentDirectory(null));
        }

        [Fact]
        public void FileHelper_MoveToParentDirectory_WithValidArgument_MovesContentToParentDirectory()
        {
            FileHelper.MoveToParentDirectory("./baz/dir1");

            Assert.False(Directory.Exists("./baz/dir1"));
            string[] files = Directory.GetFiles(".\\baz", "*", SearchOption.AllDirectories);
            Assert.Equal(new[] {
                ".\\baz\\file0.txt",
                ".\\baz\\dir2\\file1.txt",
                ".\\baz\\dir2\\file2.txt",
                ".\\baz\\dir4\\file5.txt",
                ".\\baz\\dir2\\dir3\\file3.txt",
                ".\\baz\\dir2\\dir3\\file4.txt",
            }, files);
        }

        [Fact]
        public void FileHelper_CopyIncrement_WithNullOrWhitespaceArguments_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => FileHelper.CopyIncrement(null, "./foo/ame.txt"));
            Assert.Throws<ArgumentNullException>(() => FileHelper.CopyIncrement("./foo/ame.txt", null));

            Assert.Throws<ArgumentException>(() => FileHelper.CopyIncrement("", "./foo/ame.txt"));
            Assert.Throws<ArgumentException>(() => FileHelper.CopyIncrement("./foo/ame.txt", ""));

            Assert.Throws<ArgumentException>(() => FileHelper.CopyIncrement(" ", "./foo/ame.txt"));
            Assert.Throws<ArgumentException>(() => FileHelper.CopyIncrement("./foo/ame.txt", " "));
        }

        [Fact]
        public void FileHelper_CopyIncrement_WithNotExistingSource_ThrowsException()
        {
            Assert.Throws<FileNotFoundException>(() => FileHelper.CopyIncrement("./foo/totoro.txt", "foo"));
        }

        [Fact]
        public void FileHelper_CopyIncrement_WithExistingSource_CopiesAndIncrementIfNeeded()
        {
            FileHelper.CopyIncrement("./foo/ame.txt", "./bar/foobar.txt");
            FileHelper.CopyIncrement("./foo/ame.txt", "./bar/ame.txt");
            FileHelper.CopyIncrement("./foo/ame.txt", "./bar/ame.txt");
            FileHelper.CopyIncrement("./foo/ame.txt", "./bar/ame.txt");
            FileHelper.CopyIncrement("./foo/yuki.txt", "./bar/yuki.txt");
            FileHelper.CopyIncrement("./foo/yuki.txt", "./bar/yuki.txt");

            string[] files = Directory.GetFiles("./bar").Select(Path.GetFileName).ToArray();

            Assert.Equal(new[] {
                "ame (2).txt",
                "ame (3).txt",
                "ame.txt",
                "foobar.txt",
                "yuki (2).txt",
                "yuki.txt",
            }, files);
        }

        [Fact]
        public void FileHelper_WriteAllTextIncrement_WithNullOrWhitespacePath_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => FileHelper.WriteAllTextIncrement(null, "foo bar"));
            Assert.Throws<ArgumentException>(() => FileHelper.WriteAllTextIncrement("", "foo bar"));
            Assert.Throws<ArgumentException>(() => FileHelper.WriteAllTextIncrement(" ", "foo bar"));
        }

        [Fact]
        public void FileHelper_WriteAllTextIncrement_WithNonNullOrWhitespacePath_WritesAllTextAndIncrementsIfNeeded()
        {
            FileHelper.WriteAllTextIncrement("./bar/foobar.txt", "foo bar");
            FileHelper.WriteAllTextIncrement("./bar/ame.txt", "foo bar");
            FileHelper.WriteAllTextIncrement("./bar/ame.txt", "foo bar");
            FileHelper.WriteAllTextIncrement("./bar/ame.txt", "foo bar");
            FileHelper.WriteAllTextIncrement("./bar/yuki.txt", "foo bar");
            FileHelper.WriteAllTextIncrement("./bar/yuki.txt", "foo bar");

            string[] files = Directory.GetFiles("./bar").Select(Path.GetFileName).ToArray();

            Assert.Equal(new[] {
                "ame (2).txt",
                "ame (3).txt",
                "ame.txt",
                "foobar.txt",
                "yuki (2).txt",
                "yuki.txt",
            }, files);
        }

        [Fact]
        public void FileHelper_GetIncrementedPath_WithNullOrWhitespacePath_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => FileHelper.GetIncrementedPath(null));
            Assert.Throws<ArgumentException>(() => FileHelper.GetIncrementedPath(""));
            Assert.Throws<ArgumentException>(() => FileHelper.GetIncrementedPath(" "));
        }

        [Fact]
        public void FileHelper_GetIncrementedPath_WithNonNullOrWhitespacePath_WritesAllTextAndIncrementsIfNeeded()
        {
            File.WriteAllText("./bar/ame.txt", "");
            File.WriteAllText("./bar/ame (2).txt", "");
            File.WriteAllText("./bar/yuki.txt", "");

            string path1 = FileHelper.GetIncrementedPath("./bar/foobar.txt");
            string path2 = FileHelper.GetIncrementedPath("./bar/ame.txt");
            string path3 = FileHelper.GetIncrementedPath("./bar/yuki.txt");

            Assert.Equal(@"./bar/foobar.txt", path1);
            Assert.Equal(@".\bar\ame (3).txt", path2);
            Assert.Equal(@".\bar\yuki (2).txt", path3);
        }

        public void Dispose()
        {
            Directory.Delete("./foo", true);
            Directory.Delete("./bar", true);
            Directory.Delete("./baz", true);
        }
    }
}
