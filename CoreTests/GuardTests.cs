using System;
using System.IO;
using System.Text.RegularExpressions;
using Supernova.Foundation.Core;
using Supernova.Foundation.Core.Exceptions;
using Xunit;

namespace Supernova.Foundation.CoreTests
{
    public class GuardTests
    {
        public GuardTests()
        {
            Directory.CreateDirectory("./existing_directory");
            File.WriteAllText("./existing_file.txt", "foo bar");
        }

        [Fact]
        public void Guard_Negative_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.Negative((IComparable)null, ""));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(0.01)]
        [InlineData("a")]
        [InlineData('a')]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData(true)]
        public void Guard_Negative_WithStricltlyPositiveArgument_ThrowsException<T>(T argument) where T : IComparable
        {
            Assert.Throws<ArgumentException>(() => Guard.Negative(argument, ""));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-0.01)]
        [InlineData(false)]
        public void Guard_Negative_WithNegativeArgument_DoesNotThrow<T>(T argument) where T : IComparable
        {
            Guard.Negative(argument, "");
        }

        [Fact]
        public void Guard_NotNull_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.NotNull((object)null, ""));
        }

        [Fact]
        public void Guard_NotNull_WithNotNullArgument_DoesNotThrow()
        {
            Guard.NotNull(new object(), "");
        }

        [Fact]
        public void Guard_NotNullOrEmpty_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.NotNullOrEmpty((string)null, ""));
        }

        [Fact]
        public void Guard_NotNullOrEmpty_WithEmptyArgument_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Guard.NotNullOrEmpty(new int[0], ""));
        }

        [Fact]
        public void Guard_NotNullOrEmpty_WithNotNullOrEmptyArgument_DoesNotThrow()
        {
            Guard.NotNullOrEmpty(" ", "");
        }

        [Fact]
        public void Guard_NotNullOrWhitespace_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.NotNullOrWhitespace(null, ""));
        }

        [Fact]
        public void Guard_NotNullOrWhitespace_WithEmptyArgument_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Guard.NotNullOrWhitespace("", ""));
        }

        [Fact]
        public void Guard_NotNullOrWhitespace_WithWhitespaceArgument_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Guard.NotNullOrWhitespace(" ", ""));
        }

        [Fact]
        public void Guard_NotNullOrWhitespace_WithNotNullOrEmptyArgument_DoesNotThrow()
        {
            Guard.NotNullOrWhitespace("a b", "");
        }

        [Fact]
        public void Guard_Positive_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.Positive((IComparable)null, ""));
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-0.01)]
        public void Guard_Positive_WithStrictlyNegativeArgument_ThrowsException<T>(T argument) where T : IComparable
        {
            Assert.Throws<ArgumentException>(() => Guard.Positive(argument, ""));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(0.01)]
        [InlineData("a")]
        [InlineData('a')]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData(true)]
        [InlineData(false)]
        public void Guard_Positive_WithPositiveArgument_DoesNotThrow<T>(T argument) where T : IComparable
        {
            Guard.Positive(argument, "");
        }

        [Fact]
        public void Guard_Require_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.Require(null, ""));
        }

        [Fact]
        public void Guard_Require_WithFalseCondition_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Guard.Require(false, ""));
            Assert.Throws<ArgumentException>(() => Guard.Require(() => false, ""));
        }

        [Fact]
        public void Guard_Require_WithTrueCondition_DoesNotThrow()
        {
            Guard.Require(true, "");
            Guard.Require(() => true, "");
        }

        [Fact]
        public void Guard_StrictlyNegative_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.StrictlyNegative((IComparable)null, ""));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(0.01)]
        [InlineData("a")]
        [InlineData('a')]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData(true)]
        [InlineData(false)]
        public void Guard_StrictlyNegative_WithPositiveArgument_ThrowsException<T>(T argument) where T : IComparable
        {
            Assert.Throws<ArgumentException>(() => Guard.StrictlyNegative(argument, ""));
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-0.01)]
        public void Guard_StrictlyNegative_WithStrictlyNegativeArgument_DoesNotThrow<T>(T argument) where T : IComparable
        {
            Guard.StrictlyNegative(argument, "");
        }

        [Fact]
        public void Guard_StrictlyPositive_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.StrictlyPositive((IComparable)null, ""));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-0.01)]
        [InlineData(false)]
        public void Guard_StrictlyPositive_WithNegativeArgument_ThrowsException<T>(T argument) where T : IComparable
        {
            Assert.Throws<ArgumentException>(() => Guard.StrictlyPositive(argument, ""));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(0.01)]
        [InlineData("a")]
        [InlineData('a')]
        [InlineData(" ")]
        [InlineData("")]
        [InlineData(true)]
        public void Guard_StrictlyPositive_WithStrictlyPositiveArgument_DoesNotThrow<T>(T argument) where T : IComparable
        {
            Guard.StrictlyPositive(argument, "");
        }

        [Fact]
        public void Guard_FileExist_WithNotExistingFile_ThrowsException()
        {
            Assert.Throws<FileNotFoundException>(() => Guard.FileExist("not_existing_file.txt"));
        }

        [Fact]
        public void Guard_FileExist_WithExistingFile_DoesNotThrow()
        {
            Guard.FileExist("./existing_file.txt");
        }

        [Fact]
        public void Guard_DirectoryExist_WithNotExistingDirectory_ThrowsException()
        {
            Assert.Throws<DirectoryNotFoundException>(() => Guard.DirectoryExist("not_existing_directory"));
        }

        [Fact]
        public void Guard_DirectoryExist_WithExistingDirectory_DoesNotThrow()
        {
            Guard.DirectoryExist("./existing_directory");
        }

        [Fact]
        public void Guard_MinimumCount_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.MinimumCount(2, null as int[], ""));
        }

        [Theory]
        [InlineData(1, new int[0])]
        [InlineData(4, new int[] { 1, 2, 3 })]
        public void Guard_MinimumCount_WithNotEnoughItems_ThrowsException(int count, int[] arg)
        {
            Assert.Throws<ArgumentException>(() => Guard.MinimumCount(count, arg, ""));
        }

        [Theory]
        [InlineData(-1, new int[0])]
        [InlineData(2, new int[] { 1, 2, 3 })]
        [InlineData(3, new int[] { 1, 2, 3 })]
        public void Guard_MinimumCount_WithNotEnoughItems_DoesNotThrow(int count, int[] arg)
        {
            Guard.MinimumCount(count, arg, "");
        }

        [Fact]
        public void Guard_InRange_WithNullArguments_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.InRange<IComparable>(null, 10, 5, ""));
            Assert.Throws<ArgumentNullException>(() => Guard.InRange<IComparable>(1, null, 5, ""));
            Assert.Throws<ArgumentNullException>(() => Guard.InRange<IComparable>(1, 10, null, ""));
        }

        [Theory]
        [InlineData(1, 10, -1)]
        [InlineData(1, 10, 0)]
        [InlineData(1, 10, 11)]
        [InlineData(1, 10, 12)]
        public void Guard_InRange_WithOutOfRangeArgument_ThrowsException(int lower, int upper, int arg)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Guard.InRange(lower, upper, arg, ""));
        }

        [Theory]
        [InlineData(1, 10, 1)]
        [InlineData(1, 10, 2)]
        [InlineData(1, 10, 9)]
        [InlineData(1, 10, 10)]
        public void Guard_InRange_WithInRangeArgument_DoesNotThrow(int lower, int upper, int arg)
        {
            Guard.InRange(lower, upper, arg, "");
        }

        [Fact]
        public void Guard_Match_WithNullArguments_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.Match(null, "a", "a"));
            Assert.Throws<ArgumentNullException>(() => Guard.Match("a", null, "a"));
            Assert.Throws<ArgumentNullException>(() => Guard.Match("a", "a", null));
        }

        [Fact]
        public void Guard_Match_WithWhitespaceArguments_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Guard.Match(" ", "a", "a"));
            Assert.Throws<ArgumentException>(() => Guard.Match("a", " ", "a"));
            Assert.Throws<ArgumentException>(() => Guard.Match("a", "a", " "));
        }

        [Theory]
        [InlineData("1D100", @"\d+d\d+", RegexOptions.None)]
        [InlineData("d100", @"\d+d\d+", RegexOptions.None)]
        public void Guard_Match_WithNoMatch_ThrowsException(string argument, string regex, RegexOptions options)
        {
            Assert.Throws<ArgumentException>(() => Guard.Match(argument, "a", regex, options));
        }

        [Theory]
        [InlineData("1D100", @"\d+d\d+", RegexOptions.IgnoreCase)]
        [InlineData("1d100", @"\d+d\d+", RegexOptions.None)]
        public void Guard_Match_WithMatch_DoesNotThrow(string argument, string regex, RegexOptions options)
        {
            Guard.Match(argument, "a", regex, options);
        }

        [Fact]
        public void Guard_Type_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Guard.Type<string>(null, "a"));
        }

        [Fact]
        public void Guard_Type_WithNotMatchingType_ThrowsException()
        {
            Assert.Throws<ArgumentTypeException>(() => Guard.Type<string>(1, "a"));
            Assert.Throws<ArgumentTypeException>(() => Guard.Type<int>("foo", "a"));
            Assert.Throws<ArgumentTypeException>(() => Guard.Type<ClassB>(new ClassA(), "a"));
            Assert.Throws<ArgumentTypeException>(() => Guard.Type<IBar>(new ClassA(), "a"));
        }

        [Fact]
        public void Guard_Type_WithMatchingType_DoesNotThrow()
        {
            Guard.Type<string>("foo", "a");
            Guard.Type<object>("foo", "a");
            Guard.Type<int>(1, "a");
            Guard.Type<object>(1, "a");
            Guard.Type<IFoo>(new ClassA(), "a");
            Guard.Type<ClassA>(new ClassB(), "a");
            Guard.Type<IFoo>(new ClassB(), "a");
            Guard.Type<IBar>(new ClassB(), "a");
        }

        [Theory]
        [InlineData(-4, -3)]
        [InlineData(-4, 1)]
        [InlineData(0, 42)]
        [InlineData(42, 100)]
        public void Guard_StrictlyGreaterThan_WithStrictlyGreaterThanArgument_DoesNotThrow(int value, int argument)
        {
            Guard.StrictlyGreaterThan(value, argument, nameof(argument));
        }

        [Theory]
        [InlineData(-3, -4)]
        [InlineData(1, -4)]
        [InlineData(42, 0)]
        [InlineData(100, 42)]
        [InlineData(42, 42)]
        public void Guard_StrictlyGreaterThan_WithLowerThanOrEqualToArgument_ThrowsArgumentException(int value, int argument)
        {
            Assert.Throws<ArgumentException>(() => Guard.StrictlyGreaterThan(value, argument, nameof(argument)));
        }

        [Theory]
        [InlineData(-4, -3)]
        [InlineData(-4, 1)]
        [InlineData(0, 42)]
        [InlineData(42, 100)]
        [InlineData(42, 42)]
        public void Guard_GreaterThanOrEqualTo_WithGreaterThanOrEqualToArgument_DoesNotThrow(int value, int argument)
        {
            Guard.GreaterThanOrEqualTo(value, argument, nameof(argument));
        }

        [Theory]
        [InlineData(-3, -4)]
        [InlineData(1, -4)]
        [InlineData(42, 0)]
        [InlineData(100, 42)]
        public void Guard_GreaterThanOrEqualTo_WithStrictlyLowerThanArgument_ThrowsArgumentException(int value, int argument)
        {
            Assert.Throws<ArgumentException>(() => Guard.GreaterThanOrEqualTo(value, argument, nameof(argument)));
        }

        [Theory]
        [InlineData(-4, -5)]
        [InlineData(4, 1)]
        [InlineData(42, 0)]
        [InlineData(100, 42)]
        public void Guard_StrictlyLowerThan_WithStrictlyLowerThanArgument_DoesNotThrow(int value, int argument)
        {
            Guard.StrictlyLowerThan(value, argument, nameof(argument));
        }

        [Theory]
        [InlineData(-4, -3)]
        [InlineData(-4, 1)]
        [InlineData(0, 42)]
        [InlineData(42, 100)]
        [InlineData(42, 42)]
        public void Guard_StrictlyLowerThan_WithGreaterThanOrEqualToArgument_ThrowsArgumentException(int value, int argument)
        {
            Assert.Throws<ArgumentException>(() => Guard.StrictlyLowerThan(value, argument, nameof(argument)));
        }

        [Theory]
        [InlineData(-3, -4)]
        [InlineData(1, -4)]
        [InlineData(42, 0)]
        [InlineData(100, 42)]
        [InlineData(42, 42)]
        public void Guard_LowerThanOrEqualTo_WithLowerThanOrEqualToArgument_DoesNotThrow(int value, int argument)
        {
            Guard.LowerThanOrEqualTo(value, argument, nameof(argument));
        }

        [Theory]
        [InlineData(-4, -3)]
        [InlineData(-4, 1)]
        [InlineData(0, 42)]
        [InlineData(42, 100)]
        public void Guard_LowerThanOrEqualTo_WithStrictlyGreaterThanArgument_ThrowsArgumentException(int value, int argument)
        {
            Assert.Throws<ArgumentException>(() => Guard.LowerThanOrEqualTo(value, argument, nameof(argument)));
        }

        private interface IFoo{ }

        private interface IBar { }

        private class ClassA : IFoo{ }

        private class ClassB : ClassA , IBar{ }
    }

}
