﻿using Supernova.Foundation.Core;
using Xunit;

namespace Supernova.Foundation.CoreTests
{
    public class StringProducerTests
    {
        [Theory]
        [InlineData(0, 0, "0")]
        [InlineData(-10, -45, "-45–-10")]
        [InlineData(2016, 2019, "2016–2019")]
        [InlineData(2019, 2016, "2016–2019")]
        [InlineData(2019, 2019, "2019")]
        public void StringProducer_YearsRange_ProducesYearsRange(int year1, int year2, string expected)
        {
            string range = StringProducer.YearsRange(year1, year2);
            Assert.Equal(expected, range);
        }
    }
}
