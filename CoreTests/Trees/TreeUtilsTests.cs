using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Supernova.Foundation.Core.Trees;
using Xunit;

namespace Supernova.Foundation.CoreTests.Trees
{
    public class TreeUtilsTests
    {
        [Fact]
        public void TreeUtils_Flatten_WithNullRoot_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => TreeUtils.Flatten<string>(null, s => new[] { "foo", "bar" }));
        }

        [Fact]
        public void TreeUtils_Flatten_WithNullNodesSelector_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => TreeUtils.Flatten(" ", null));
        }

        [Fact]
        public void TreeUtils_Flatten_WithTreeAndNodesSelector_ReturnsFlattenedNodes()
        {
            Node tree = GetTree();

            HashSet<Node> result = TreeUtils.Flatten(tree, n => n.Children);

            Assert.Equal(7, result.Count);
            Assert.Equal(new[] { 0, 1, 3, 4, 2, 5, 6 }, result.Select(n => n.Id));
        }

        private Node GetTree()
        {
            var node6 = new Node(6);
            var node5 = new Node(5) { Children = new List<Node> { node6 } };
            var node4 = new Node(4);
            var node3 = new Node(3);
            var node2 = new Node(2) { Children = new List<Node> { node4, node5 } };
            var node1 = new Node(1) { Children = new List<Node> { node3, node4 } };
            node3.Children.Add(node1);

            return new Node(0) { Children = new List<Node> { node1, node2 } };
        }

        [DebuggerDisplay("Id = {Id}")]
        private class Node
        {
            public Node(int id)
            {
                Id = id;
            }

            public int Id { get; set; }

            public List<Node> Children { get; set; } = new List<Node>();
        }
    }
}
