﻿using Supernova.Foundation.Core.Stringification;
using Xunit;

namespace Supernova.Foundation.CoreTests.Stringification
{
    public class DefaultTupleStringifierTests
    {
        [Theory]
        [InlineData(613, 42, "(613, 42)")]
        [InlineData("foo", "bar", "(foo, bar)")]
        public void DefaultTupleStringifier_Stringify_ReturnsStringRepresentation<T1, T2>(T1 t1, T2 t2, string expected)
        {
            string result = new DefaultTupleStringifier<T1, T2>().Stringify((t1, t2));
            Assert.Equal(expected, result);
        }
    }
}
