﻿using Supernova.Foundation.Core.Stringification;
using Xunit;

namespace Supernova.Foundation.CoreTests.Stringification
{
    public class DefaultCollectionStringifierTests
    {
        [Fact]
        public void DefaultCollectionStringifier_Stringify_WithNullInput_ReturnsEmptyString()
        {
            Assert.Empty(new DefaultCollectionStringifier<string>().Stringify(null));
        }

        [Fact]
        public void DefaultCollectionStringifier_Stringify_WithEmptyCollection_ReturnsEmptyString()
        {
            Assert.Empty(new DefaultCollectionStringifier<string>().Stringify(new string[0]));
        }

        [Fact]
        public void DefaultCollectionStringifier_Stringify_WithNotEmptyCollection_ReturnsStringRepresentation()
        {
            int[] collection1 = new[] { 4, 8, 15, 16, 23, 42 };
            string[] collection2 = new[] { "foo", "bar", "baz" };
            object[] collection3 = new object[] { 4, "foo", 8, "bar", 15, "baz" };
            object[] collection4 = new object[]
            {
                new { Foo = "Foo", Bar = "Bar" },
                new { Baz = "Baz"}
            };

            string result1 = new DefaultCollectionStringifier<int>().Stringify(collection1);
            string result2 = new DefaultCollectionStringifier<string>().Stringify(collection2);
            string result3 = new DefaultCollectionStringifier<object>().Stringify(collection3);
            string result4 = new DefaultCollectionStringifier<object>().Stringify(collection4);

            Assert.Equal("[4, 8, 15, 16, 23, 42]", result1);
            Assert.Equal("[foo, bar, baz]", result2);
            Assert.Equal("[4, foo, 8, bar, 15, baz]", result3);
            Assert.Equal("[{ Foo = Foo, Bar = Bar }, { Baz = Baz }]", result4);
        }
    }
}
