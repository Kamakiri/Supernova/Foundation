﻿using System.Collections.Specialized;
using Supernova.Foundation.Core.Collections;
using Xunit;

namespace Supernova.Foundation.CoreTests.Collections
{
    public class ObservableStackTests
    {
        [Fact]
        public void ObservableStack_Push_CallsCollectionChangedEvent()
        {
            int calls = 0;
            var stack = new ObservableStack<int>(new[] { 42, 43 });
            stack.CollectionChanged += (o, e) =>
            {
                calls++;
                Assert.Null(e.OldItems);
                Assert.Equal(1, e.NewItems.Count);
                Assert.Equal(calls == 1 ? 4 : calls == 2 ? 8 : 15, e.NewItems[0]);
                Assert.Equal(NotifyCollectionChangedAction.Add, e.Action);
            };

            stack.Push(4);
            stack.Push(8);
            stack.Push(15);

            Assert.Equal(3, calls);
            Assert.Equal(new[] { 15, 8, 4, 43, 42 }, stack);
        }

        [Fact]
        public void ObservableStack_Push_CallsPropertyChangedEvent()
        {
            int calls = 0;
            var stack = new ObservableStack<int>(new[] { 42, 43 });
            stack.PropertyChanged += (o, e) =>
            {
                calls++;
                Assert.Equal(nameof(stack.Count), e.PropertyName);
            };

            stack.Push(4);
            stack.Push(8);
            stack.Push(15);

            Assert.Equal(3, calls);
        }

        [Fact]
        public void ObservableStack_Pop_CallsCollectionChangedEvent()
        {
            int calls = 0;
            var stack = new ObservableStack<int>(new[] { 42, 43, 4, 8, 15 });
            stack.CollectionChanged += (o, e) =>
            {
                calls++;
                Assert.Null(e.NewItems);
                Assert.Equal(1, e.OldItems.Count);
                Assert.Equal(calls == 1 ? 15 : calls == 2 ? 8 : 4, e.OldItems[0]);
                Assert.Equal(NotifyCollectionChangedAction.Remove, e.Action);
            };

            stack.Pop();
            stack.Pop();
            stack.Pop();

            Assert.Equal(3, calls);
            Assert.Equal(new[] { 43, 42 }, stack);
        }

        [Fact]
        public void ObservableStack_Pop_CallsPropertyChangedEvent()
        {
            int calls = 0;
            var stack = new ObservableStack<int>(new[] { 42, 43, 4, 8, 15 });
            stack.PropertyChanged += (o, e) =>
            {
                calls++;
                Assert.Equal(nameof(stack.Count), e.PropertyName);
            };

            stack.Pop();
            stack.Pop();
            stack.Pop();

            Assert.Equal(3, calls);
        }

        [Fact]
        public void ObservableStack_Clear_CallsCollectionChangedEvent()
        {
            int calls = 0;
            var stack = new ObservableStack<int>(new[] { 42, 43, 4, 8, 15 });
            stack.CollectionChanged += (o, e) =>
            {
                calls++;
                Assert.Null(e.NewItems);
                Assert.Null(e.OldItems);
                Assert.Equal(NotifyCollectionChangedAction.Reset, e.Action);
            };

            stack.Clear();

            Assert.Equal(1, calls);
            Assert.Empty(stack);
        }

        [Fact]
        public void ObservableStack_Clear_CallsPropertyChangedEvent()
        {
            int calls = 0;
            var stack = new ObservableStack<int>(new[] { 42, 43, 4, 8, 15 });
            stack.PropertyChanged += (o, e) =>
            {
                calls++;
                Assert.Equal(nameof(stack.Count), e.PropertyName);
            };

            stack.Clear();

            Assert.Equal(1, calls);
        }
    }
}
