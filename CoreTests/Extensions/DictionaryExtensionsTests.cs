using System;
using System.Collections.Generic;
using Supernova.Foundation.Core.Extensions;
using Xunit;

namespace Supernova.Foundation.CoreTests.Extensions
{
    public class DictionaryExtensionsTests
    {
        [Fact]
        public void DictionaryExtensions_AddOrUpdate_WithNullDictionary_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => DictionaryExtensions.AddOrUpdate(null, "key", "value"));
        }

        [Fact]
        public void DictionaryExtensions_AddOrUpdate_WithNullKey_ThrowsException()
        {
            var dic = new Dictionary<string, string>();
            Assert.Throws<ArgumentNullException>(() => dic.AddOrUpdate(null, "value"));
        }

        [Fact]
        public void DictionaryExtensions_AddOrUpdate_WithNonExistingKey_AddsNewEntryAndReturnsTrue()
        {
            // Arrange
            var dic = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2"
            };

            // Act
            bool result = dic.AddOrUpdate("key3", "value3");

            // Assert
            Assert.True(result);

            var expected = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2",
                ["key3"] = "value3"
            };
            Assert.Equal(expected, dic);
        }

        [Fact]
        public void DictionaryExtensions_AddOrUpdate_WithExistingKey_UpdatesEntryAndReturnsFalse()
        {
            // Arrange
            var dic = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2",
                ["key3"] = "value3",
            };

            // Act
            bool result1 = dic.AddOrUpdate("key2", null);
            bool result2 = dic.AddOrUpdate("key3", "value3.1");

            // Assert
            Assert.False(result1);
            Assert.False(result2);

            var expected = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = null,
                ["key3"] = "value3.1"
            };
            Assert.Equal(expected, dic);
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithNullFirstDictionary_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => DictionaryExtensions.Merge(null, new Dictionary<string, string>(), (a, b) => a));
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithNullSecondDictionary_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => new Dictionary<string, string>().Merge(null, (a, b) => a));
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithNullResolutionFunction_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => new Dictionary<string, string>().Merge(new Dictionary<string, string>(), null));
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithEmptyDictionaries_ReturnsEmptyDictionary()
        {
            var merge = new Dictionary<string, string>().Merge(new Dictionary<string, string>(), (a, b) => a);

            Assert.Empty(merge);
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithEmptyFirstDictionary_ReturnsSecondDictionaryClone()
        {
            // Arrange
            var first = new Dictionary<string, string>();
            var second = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2",
                ["key3"] = "value3"
            };

            // Act
            Dictionary<string, string> merge = first.Merge(second, (a, b) => a);

            // Assert
            Assert.Equal(second, merge);
            Assert.NotSame(second, merge);
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithEmptySecondDictionary_ReturnsFirstDictionaryClone()
        {
            // Arrange
            var first = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2",
                ["key3"] = "value3"
            };
            var second = new Dictionary<string, string>();

            // Act
            Dictionary<string, string> merge = first.Merge(second, (a, b) => a);

            // Assert
            Assert.Equal(first, merge);
            Assert.NotSame(first, merge);
        }

        [Fact]
        public void DictionaryExtensions_Merge_WithNotEmptyDictionaries_ReturnsMerge()
        {
            // Arrange
            var first = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2",
                ["key3"] = "value3"
            };
            var second = new Dictionary<string, string>
            {
                ["key2"] = "foo",
                ["key3"] = "bar",
                ["key4"] = "baz"
            };

            // Act
            Dictionary<string, string> merge = first.Merge(second, (a, b) => a + b);

            // Assert
            var expected = new Dictionary<string, string>
            {
                ["key1"] = "value1",
                ["key2"] = "value2foo",
                ["key3"] = "value3bar",
                ["key4"] = "baz"
            };
            Assert.Equal(expected, merge);
            Assert.NotSame(first, merge);
            Assert.NotSame(second, merge);
        }
    }
}
