﻿using System;
using System.Globalization;
using Supernova.Foundation.Core.Extensions;
using Xunit;

namespace Supernova.Foundation.CoreTests.Extensions
{
    public class StringExtensionsTests
    {
        [Fact]
        public void StringExtensions_Repeat_WithNullString_ReturnsEmptyString()
        {
            string result = (null as string).Repeat(1);

            Assert.Empty(result);
        }

        [Fact]
        public void StringExtensions_Repeat_WithNegativeRepeat_ThrowsException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => "foo".Repeat(-1));
        }

        [Fact]
        public void StringExtensions_Repeat_WithZeroRepeat_ReturnsEmptyString()
        {
            string result = "foo".Repeat(0);

            Assert.Empty(result);
        }

        [Fact]
        public void StringExtensions_Repeat_WithStrictlyPositiveRepeat_ReturnsRepeatedString()
        {
            string result = "foo".Repeat(3);

            Assert.Equal("foofoofoo", result);
        }

        [Fact]
        public void StringExtensions_ToPascalCase_WithNullString_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => ((string)null).ToPascalCase());
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("fooBar", "Foobar")]
        [InlineData("FooBar", "Foobar")]
        [InlineData("foo-bar", "FooBar")]
        [InlineData(@"foo,bar:baz-qux.quux–quuz|corge—grault\garply/waldo	fred", "FooBarBazQuxQuuxQuuzCorgeGraultGarplyWaldoFred")]
        public void StringExtensions_ToPascalCase_WithNonNullString_ReturnsPascalCaseString(string input, string expected)
        {
            Assert.Equal(expected, input.ToPascalCase());
        }

        [Fact]
        public void StringExtensions_ToPascalCase_WithNonNullStringAndCulture_ReturnsPascalCaseStringAdaptedToCulture()
        {
            Assert.Equal("IiiIii", "iii-iIi".ToPascalCase());
            Assert.Equal("İiiİii", "iii-iİi".ToPascalCase(new CultureInfo("tr-TR")));
        }

        [Fact]
        public void StringExtensions_ToCamelCase_WithNullString_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => ((string)null).ToCamelCase());
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("fooBar", "foobar")]
        [InlineData("FooBar", "foobar")]
        [InlineData("foo-bar", "fooBar")]
        [InlineData(@"foo,bar:baz-qux.quux–quuz|corge—grault\garply/waldo	fred", "fooBarBazQuxQuuxQuuzCorgeGraultGarplyWaldoFred")]
        public void StringExtensions_ToCamelCase_WithNonNullString_ReturnsCamelCaseString(string input, string expected)
        {
            Assert.Equal(expected, input.ToCamelCase());
        }

        [Fact]
        public void StringExtensions_ToCamelCase_WithNonNullStringAndCulture_ReturnsCamelCaseStringAdaptedToCulture()
        {
            Assert.Equal("iiiIii", "iii-iIi".ToCamelCase());
            Assert.Equal("iiiİii", "iii-iİi".ToCamelCase(new CultureInfo("tr-TR")));
        }

        [Fact]
        public void StringExtensions_ToSnakeCase_WithNullString_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => ((string)null).ToSnakeCase());
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("fooBar", "foobar")]
        [InlineData("FooBar", "foobar")]
        [InlineData("foo-bar", "foo_bar")]
        [InlineData(@"foo,bar:baz-qux.quux–quuz|corge—grault\garply/waldo	fred", "foo_bar_baz_qux_quux_quuz_corge_grault_garply_waldo_fred")]
        public void StringExtensions_ToSnakeCase_WithNonNullString_ReturnsSnakeCaseString(string input, string expected)
        {
            Assert.Equal(expected, input.ToSnakeCase());
        }

        [Fact]
        public void StringExtensions_ToSnakeCase_WithNonNullStringAndCulture_ReturnsSnakeCaseStringAdaptedToCulture()
        {
            Assert.Equal("iii_iii", "iii-iIi".ToSnakeCase());
            Assert.Equal("iii_iii", "iii-iİi".ToSnakeCase(new CultureInfo("tr-TR")));
        }

        [Fact]
        public void StringExtensions_ToKebabCase_WithNullString_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => ((string)null).ToKebabCase());
        }

        [Theory]
        [InlineData("", "")]
        [InlineData("fooBar", "foobar")]
        [InlineData("FooBar", "foobar")]
        [InlineData("foo_bar", "foo-bar")]
        [InlineData(@"foo,bar:baz-qux.quux–quuz|corge—grault\garply/waldo	fred", "foo-bar-baz-qux-quux-quuz-corge-grault-garply-waldo-fred")]
        public void StringExtensions_ToKebabCase_WithNonNullString_ReturnsKebabCaseString(string input, string expected)
        {
            Assert.Equal(expected, input.ToKebabCase());
        }

        [Fact]
        public void StringExtensions_ToKebabCase_WithNonNullStringAndCulture_ReturnsKebabCaseStringAdaptedToCulture()
        {
            Assert.Equal("iii-iii", "iii_iIi".ToKebabCase());
            Assert.Equal("iii-iii", "iii_iİi".ToKebabCase(new CultureInfo("tr-TR")));
        }
    }
}
