using System;
using System.Collections.Generic;
using System.Linq;
using Supernova.Foundation.Core.Extensions;
using Xunit;

namespace Supernova.Foundation.CoreTests.Extensions
{
    public class EnumerableExtensionsTests
    {
        [Fact]
        public void EnumerableExtensions_FirstNotIn_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.FirstNotIn(null));
        }

        [Fact]
        public void EnumerableExtensions_FirstNotIn_WithEmptyArgument_ReturnsStart()
        {
            Assert.Equal(0, new int[0].FirstNotIn());
            Assert.Equal(-1, new int[0].FirstNotIn(-1));
            Assert.Equal(42, new int[0].FirstNotIn(42));
        }

        [Theory]
        [InlineData(new[] { 1 }, -1, -1)]
        [InlineData(new[] { 1 }, 0, 0)]
        [InlineData(new[] { 1 }, 1, 2)]
        [InlineData(new[] { 1 }, 42, 42)]
        [InlineData(new[] { 1, 2, 4 }, -1, -1)]
        [InlineData(new[] { 1, 2, 4 }, 0, 0)]
        [InlineData(new[] { 1, 2, 4 }, 1, 3)]
        [InlineData(new[] { 1, 2, 4 }, 2, 3)]
        [InlineData(new[] { 1, 2, 4 }, 3, 3)]
        [InlineData(new[] { 1, 2, 4 }, 4, 5)]
        [InlineData(new[] { 1, 2, 4 }, 42, 42)]
        [InlineData(new[] { 1, 2, 4, 5, 6 }, 2, 3)]
        [InlineData(new[] { 1, 2, 4, 5, 6 }, 3, 3)]
        [InlineData(new[] { 1, 2, 4, 5, 6 }, 4, 7)]
        public void EnumerableExtensions_FirstNotIn_ReturnsResult(int[] input, int start, int expected)
        {
            Assert.Equal(expected, input.FirstNotIn(start));
        }

        [Fact]
        public void EnumerableExtensions_GetAndRemoveAt_WithNullList_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.GetAndRemoveAt(null as int[], 0));
        }

        [Fact]
        public void EnumerableExtensions_GetAndRemoveAt_WithEmptyList_ThrowsException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => EnumerableExtensions.GetAndRemoveAt(new int[0], 0));
        }

        [Fact]
        public void EnumerableExtensions_GetAndRemoveAt_WithNotEmptyList_GetAndRemoveElement()
        {
            // Arrange
            var list = new List<string> { "foo", "bar", "baz" };

            // Act
            string element = list.GetAndRemoveAt(1);

            // Assert
            Assert.Equal("bar", element);
            Assert.Equal(new List<string> { "foo", "baz" }, list);
        }

        [Fact]
        public void EnumerableExtensions_HasAtLeast_WithNullList_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.HasAtLeast(null as ICollection<int>, 1));
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.HasAtLeast(null as IEnumerable<int>, 1));
        }

        [Theory]
        [InlineData(new[] { "foo", "bar", "baz" }, -1, true)]
        [InlineData(new[] { "foo", "bar", "baz" }, 0, true)]
        [InlineData(new[] { "foo", "bar", "baz" }, 1, true)]
        [InlineData(new[] { "foo", "bar", "baz" }, 3, true)]
        [InlineData(new[] { "foo", "bar", "baz" }, 4, false)]
        [InlineData(new string[0], -1, true)]
        [InlineData(new string[0], 0, true)]
        [InlineData(new string[0], 1, false)]
        public void EnumerableExtensions_HasAtLeast_WithNotNullList_ReturnsExpectedResult(ICollection<string> collection, int count, bool expected)
        {
            // Act
            bool result1 = collection.HasAtLeast(count);
            bool result2 = (collection as IEnumerable<string>).HasAtLeast(count);

            // Assert
            Assert.Equal(expected, result1);
            Assert.Equal(expected, result2);
        }

        [Fact]
        public void EnumerableExtensions_Apply_WithNullEnumerable_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.Apply<string>(null, i => { }).ToList());
        }

        [Fact]
        public void EnumerableExtensions_Apply_WithNullAction_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => new[] { "foo", "bar", "baz" }.Apply(null).ToList());
        }

        [Fact]
        public void EnumerableExtensions_Apply_WithNotNullAction_AppliesWithDeferedExecution()
        {
            // Arrange
            var array = new[] { "foo", "bar", "baz" };
            var list = new List<string>();
            void Action(string s) => list.Add(s.ToUpper());

            // Act
            IEnumerable<string> enumerable = array.Apply(Action);

            // Assert
            Assert.Empty(list);

            // Act
            List<string> result = enumerable.ToList();

            // Assert
            Assert.Equal(array, result);
            Assert.Equal(new[] { "FOO", "BAR", "BAZ" }, list);
        }

        [Fact]
        public void EnumerableExtensions_Interlace_WithNullFirstEnumerable_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.Interlace(null, new string[0]).ToList());
        }

        [Fact]
        public void EnumerableExtensions_Interlace_WithNullSecondEnumerable_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => new string[0].Interlace(null).ToList());
        }

        [Fact]
        public void EnumerableExtensions_Interlace_WithEmptyFirstEnumerable_ReturnsSecondEnumerable()
        {
            // Arrange
            var first = new string[0];
            var second = new[] { "foo", "bar", "baz" };

            // Act
            IEnumerable<string> result = first.Interlace(second);

            // Assert
            Assert.Equal(second, result);
        }

        [Fact]
        public void EnumerableExtensions_Interlace_WithEmptySecondEnumerable_ReturnsFirstEnumerable()
        {
            // Arrange
            var first = new[] { "foo", "bar", "baz" };
            var second = new string[0];

            // Act
            IEnumerable<string> result = first.Interlace(second);

            // Assert
            Assert.Equal(first, result);
        }

        [Fact]
        public void EnumerableExtensions_Interlace_WithNotEmptyEnumerables_InterlacedEnumerables()
        {
            // Arrange
            var first = new[] { "foo", "bar", "baz" };
            var second = new[] { "qux", "quux", "corge", "grault" };

            // Act
            IEnumerable<string> result = first.Interlace(second);

            // Assert
            var expected = new[] { "foo", "qux", "bar", "quux", "baz", "corge", "grault" };
            Assert.Equal(expected, result);
        }

        [Fact]
        public void EnumerableExtensions_Swap_WithNullList_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.Swap<string>(null, 0, 1));
        }

        [Fact]
        public void EnumerableExtensions_Swap_WithNotNullList_Swaps()
        {
            // Arrange
            var list = new[] { "foo", "bar", "baz", "qux" };

            // Act
            list.Swap(0, 2);

            // Assert
            Assert.Equal(new[] { "baz", "bar", "foo", "qux" }, list);
        }

        [Fact]
        public void EnumerableExtensions_Split_WithNullArray_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => (null as int[]).Split(0));
        }

        [Fact]
        public void EnumerableExtensions_Split_WithOutOfRangeIndex_ThrowsException()
        {
            int[] array = new[] { 1, 2, 3 };
            Assert.Throws<ArgumentOutOfRangeException>(() => array.Split(-1));
            Assert.Throws<ArgumentOutOfRangeException>(() => array.Split(4));
        }

        [Theory]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 0, new int[0], new[] { 1, 2, 3, 4, 5 })]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 1, new[] { 1 }, new[] { 2, 3, 4, 5 })]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 2, new[] { 1, 2 }, new[] { 3, 4, 5 })]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 3, new[] { 1, 2, 3 }, new[] { 4, 5 })]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 4, new[] { 1, 2, 3, 4 }, new[] { 5 })]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 5, new[] { 1, 2, 3, 4, 5 }, new int[0])]
        public void EnumerableExtensions_Split_WithInRangeIndex_SplitsArray(int[] array, int index, int[] expectedFirst, int[] expectedLast)
        {
            (int[] first, int[] last) = array.Split(index);

            Assert.Equal(expectedFirst, first);
            Assert.Equal(expectedLast, last);
        }

        [Fact]
        public void EnumerableExtensions_Slice_WithNullArray_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => (null as int[]).Slice(0, 1));
        }

        [Fact]
        public void EnumerableExtensions_Slice_WithOutOfRangeIndex_ThrowsException()
        {
            int[] array = new[] { 1, 2, 3 };
            Assert.Throws<ArgumentOutOfRangeException>(() => array.Slice(-1, 1));
            Assert.Throws<ArgumentOutOfRangeException>(() => array.Slice(4, 1));
        }

        [Fact]
        public void EnumerableExtensions_Slice_WithOutOfRangeCount_ThrowsException()
        {
            int[] array = new[] { 1, 2, 3 };
            Assert.Throws<ArgumentOutOfRangeException>(() => array.Slice(0, -1));
            Assert.Throws<ArgumentOutOfRangeException>(() => array.Slice(0, 4));
        }

        [Theory]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 0, 0, new int[0])]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 1, 0, new int[0])]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 0, 1, new int[] { 1 })]
        [InlineData(new[] { 1, 2, 3, 4, 5 }, 1, 3, new int[] { 2, 3, 4 })]
        public void EnumerableExtensions_Slice_WithInRangeIndex_SlicesArray(int[] array, int index, int count, int[] expected)
        {
            int[] slice = array.Slice(index, count);

            Assert.Equal(expected, slice);
        }

        [Fact]
        public void EnumerableExtensions_IsEmpty_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => (null as IEnumerable<string>).IsEmpty());
        }

        [Fact]
        public void EnumerableExtensions_IsEmpty_WithEmptyArgument_ReturnsTrue()
        {
            // Arrange
            IEnumerable<string> Enumerable() { yield break; }
            List<string> arg1 = new List<string>();
            string[] arg2 = new string[0];
            IEnumerable<string> arg3 = Enumerable();

            // Act
            var result1 = arg1.IsEmpty();
            var result2 = arg2.IsEmpty();
            var result3 = arg3.IsEmpty();

            // Assert
            Assert.True(result1);
            Assert.True(result2);
            Assert.True(result3);
        }

        [Fact]
        public void EnumerableExtensions_IsEmpty_WithNotEmptyArgument_ReturnsTrue()
        {
            // Arrange
            IEnumerable<string> Enumerable() { yield return "foo"; }
            List<string> arg1 = new List<string> { "foo" };
            string[] arg2 = { "foo" };
            IEnumerable<string> arg3 = Enumerable();

            // Act
            var result1 = arg1.IsEmpty();
            var result2 = arg2.IsEmpty();
            var result3 = arg3.IsEmpty();

            // Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.False(result3);
        }

        [Theory]
        [InlineData(new[] { 4, 32, 5, 9, 2, 4, 8 }, true)]
        [InlineData(new[] { 4, 32, 5, 9, 2, 8, 7 }, false)]
        [InlineData(new int[0], false)]
        public void EnumerableExtensions_HasDuplicates_ChecksForDuplicates<T>(int[] array, bool expected)
        {
            Assert.Equal(expected, array.HasDuplicates());
        }
    }
}
