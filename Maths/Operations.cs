using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Supernova.Foundation.Core;
using Supernova.Foundation.Core.Extensions;

namespace Supernova.Foundation.Maths
{
    /// <summary>
    /// Provides methods for common maths operations.
    /// </summary>
    public static class Operations
    {
        private static readonly Dictionary<int, BigInteger> _factorialCache = new Dictionary<int, BigInteger>();

        /// <summary>
        /// Computes the binomial coefficient <paramref name="n"/> choose <paramref name="k"/> (aka <paramref name="k"/> among <paramref name="n"/>).
        /// </summary>
        /// <param name="n">The superset size.</param>
        /// <param name="k">The subset size.</param>
        /// <returns>The binomial coefficient.</returns>
        public static BigInteger BinomialCoefficient(int n, int k)
        {
            if (k < 0 || n < 0 || k > n) return 0;

            return Factorial(n) / (Factorial(k) * Factorial(n - k));
        }

        /// <summary>
        /// Counts the number of divisors of the provided number.
        /// </summary>
        /// <param name="num">A positive number.</param>
        /// <returns>The number of divisors.</returns>
        public static int CountDivisors(int num)
        {
            Guard.Positive(num, nameof(num));

            if (num == 0) return 0;

            int count = 0;
            int max = (int)Math.Sqrt(num);

            for (int i = 1; i <= max; i++)
            {
                if (num % i == 0) count += 2;
            }

            if (max * max == num) count--;

            return count;
        }

        /// <summary>
        /// Computes the factorial of <paramref name="n"/>.
        /// </summary>
        /// <param name="n">A positive number.</param>
        /// <returns>n!</returns>
        public static BigInteger Factorial(int n)
        {
            Guard.Positive(n, nameof(n));

            if (n < 2) return 1;

            if (_factorialCache.ContainsKey(n)) return _factorialCache[n];

            BigInteger result = n * Factorial(n - 1);
            _factorialCache.Add(n, result);
            return result;
        }

        /// <summary>
        /// Gets the first prime number following <paramref name="prime"/>.
        /// </summary>
        /// <param name="prime">A prime number.</param>
        /// <returns></returns>
        public static BigInteger GetNextPrime(BigInteger prime)
        {
            Guard.Require(IsPrime(prime), $"{nameof(prime)} must be a prime number.");

            if (prime == 2) return 3;
            if (prime % 2 == 0) prime++;

            do
            {
                prime += 2;
            } while (!IsPrime(prime));

            return prime;
        }

        /// <summary>
        /// Computes the GCD of the provided numbers.
        /// </summary>
        /// <param name="nums">The numbers.</param>
        /// <returns>The GCD.</returns>
        public static int GreatestCommonDivisor(params int[] nums)
        {
            Guard.NotNullOrEmpty(nums, nameof(nums));

            return nums.Aggregate(GreatestCommonDivisor);
        }

        /// <summary>
        /// Computes the GCD of two numbers.
        /// </summary>
        /// <param name="a">The first number.</param>
        /// <param name="b">The second number.</param>
        /// <returns>The GCD.</returns>
        public static int GreatestCommonDivisor(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);

            return b == 0 ? a : GreatestCommonDivisor(b, a % b);
        }

        /// <summary>
        /// Checks if a given number is a prime number.
        /// </summary>
        /// <param name="num">The number.</param>
        /// <returns><c>true</c> if <paramref name="num"/> is a prime number, <c>false</c> otherwise.</returns>
        public static bool IsPrime(BigInteger num)
        {
            if (num < 2) return false;
            if (num == 2) return true;
            if (num % 2 == 0) return false;
            if (num == 3) return true;

            for (BigInteger i = 3; ; i += 2)
            {
                BigInteger q = num / i;
                if (num == q * i) return false;
                if (q < i) return true;
            }
        }

        /// <summary>
        /// Checks if the three given numbers are a pythagorean triplet.
        /// </summary>
        /// <param name="a">The first number.</param>
        /// <param name="b">The second number.</param>
        /// <param name="c">The third number.</param>
        /// <returns><c>true</c> if <paramref name="a"/>, <paramref name="b"/> and <paramref name="c"/> are a pythagorean triplet.</returns>
        public static bool IsPythagoreanTriplet(BigInteger a, BigInteger b, BigInteger c) => a < b && b < c && a * a + b * b == c * c;

        /// <summary>
        /// Computes the LCM of the provided numbers.
        /// </summary>
        /// <param name="nums">The numbers.</param>
        /// <returns>The LCM.</returns>
        public static BigInteger LeastCommonMultiple(params BigInteger[] nums)
        {
            Guard.MinimumCount(2, nums, nameof(nums));

            BigInteger a = nums[0];
            BigInteger b = nums[1];

            var partialLcm = a * b / BigInteger.GreatestCommonDivisor(a, b);

            (_, BigInteger[] rest) = nums.Split(2);

            if (rest.Length == 0) return partialLcm;

            var newNums = new BigInteger[rest.Length + 1];
            newNums[0] = partialLcm;
            rest.CopyTo(newNums, 1);

            return LeastCommonMultiple(newNums);
        }

        /// <summary>
        /// Computes the multinomial coefficient <paramref name="n"/> choose <paramref name="ks"/> (aka <paramref name="ks"/> among <paramref name="n"/>).
        /// </summary>
        /// <param name="n">The superset size.</param>
        /// <param name="ks">The subsets sizes.</param>
        /// <returns>The multinomial coefficient.</returns>
        public static BigInteger MultinomialCoefficient(int n, params int[] ks)
        {
            Guard.Positive(n, nameof(n));
            Guard.NotNullOrEmpty(ks, nameof(ks));
            Guard.Require(n == ks.Sum(), "K numbers must sum up to n.");

            return Factorial(n) / ks.Select(x => Factorial(x)).Aggregate(BigInteger.Add);
        }

        /// <summary>
        /// Sums the identity series until n (1 + 2 + ... + n).
        /// If n &lt; 0, sums negative numbers (-1 - 2 - ... - n).
        /// </summary>
        /// <param name="n">The limit.</param>
        /// <returns>The sum.</returns>
        public static BigInteger SumIdentity(BigInteger n)
        {
            if (n == 0) return 0;

            BigInteger abs = BigInteger.Abs(n);
            BigInteger sign = n / abs;

            return sign * abs * (abs + 1) / 2;
        }
    }
}
