# Supernova Foundation

<img src="logo.png" width="100px" />

## Abstract

*Supernova.Foundation* is a collection of basic reusable helpers and tools.

**Work in progress**

## License

![AGPLv3](agplv3.png)

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [LICENSE.txt](LICENSE.txt) for details.

## Credits

Project icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)
