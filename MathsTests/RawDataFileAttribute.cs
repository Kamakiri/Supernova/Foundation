using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Supernova.Foundation.Core;
using Xunit.Sdk;

namespace Supernova.Foundation.MathsTests
{
    public class RawDataFileAttribute : DataAttribute
    {
        private readonly string _filename;
        private readonly bool _whitespaceSeperated;

        public RawDataFileAttribute(string filename, bool whitespaceSeperated = false)
        {
            Guard.NotNullOrWhitespace(filename, nameof(filename));

            _filename = filename;
            _whitespaceSeperated = whitespaceSeperated;
        }

        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            Guard.NotNull(testMethod, nameof(testMethod));

            string path = Path.IsPathRooted(_filename) ? _filename : Path.Combine(Directory.GetCurrentDirectory(), _filename);

            Guard.FileExist(path);

            string data = File.ReadAllText(path);

            if (!_whitespaceSeperated) return new[] { new[] { data } };

            return new[] { new[] { data.Split((string[])null, StringSplitOptions.RemoveEmptyEntries) } };
        }
    }
}
