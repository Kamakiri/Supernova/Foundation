// LICENSE NOTICE

// Copyright 2018 Kyrdairrin (Nicolas Maurice)

// This file is part of SupernovaPackages.

// SupernovaPackages is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// SupernovaPackages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with SupernovaPackages.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Supernova.Foundation.Maths;
using Xunit;

namespace Supernova.Foundation.MathsTests
{
    public class SeriesTests
    {
        [Fact]
        public void Series_Fibonacci_GeneratesFibonacciSeries()
        {
            IEnumerable<BigInteger> series = Series.Fibonacci();

            Assert.Equal(new BigInteger[] { 1 }, series.Take(1));
            Assert.Equal(new BigInteger[] { 1, 1 }, series.Take(2));
            Assert.Equal(new BigInteger[] { 1, 1, 2 }, series.Take(3));
            Assert.Equal(new BigInteger[] { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 }, series.Take(10));
        }

        [Theory]
        [RawDataFile("./10000Primes.txt", true)]
        public void Series_Primes_GeneratesPrimesSeries(string[] expected)
        {
            Assert.Equal(expected.Select(BigInteger.Parse), Series.Primes().Take(10000));
        }
    }
}
