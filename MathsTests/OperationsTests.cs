// LICENSE NOTICE

// Copyright 2018 Kyrdairrin (Nicolas Maurice)

// This file is part of SupernovaPackages.

// SupernovaPackages is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// SupernovaPackages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with SupernovaPackages.  If not, see <http://www.gnu.org/licenses/>.

// END LICENSE NOTICE
using System;
using System.Linq;
using System.Numerics;
using Supernova.Foundation.Maths;
using Xunit;

namespace Supernova.Foundation.MathsTests
{
    public class OperationsTests
    {
        [Fact]
        public void Operations_SumIdentity_WithNegativeLimit_ReturnsOppositeOfPositiveSum()
        {
            Assert.Equal(-Operations.SumIdentity(1), Operations.SumIdentity(-1));
            Assert.Equal(-Operations.SumIdentity(42), Operations.SumIdentity(-42));
        }

        [Fact]
        public void Operations_SumIdentity_ReturnsIdentitySum()
        {
            var sequence1 = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            var sequence2 = new[] { -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12 };

            Assert.Equal(sequence1.Sum(), Operations.SumIdentity(12));
            Assert.Equal(sequence2.Sum(), Operations.SumIdentity(-12));
            Assert.Equal(0, Operations.SumIdentity(0));
        }

        [Fact]
        public void Operations_IsPrime_WithPrimeNumber_ReturnsTrue()
        {
            var primes = new BigInteger[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997 };

            foreach (BigInteger prime in primes)
            {
                Assert.True(Operations.IsPrime(prime));
            }
        }

        [Fact]
        public void Operations_IsPrime_WithNonPrimeNumber_ReturnsFalse()
        {
            var nonPrimes = new BigInteger[] { -5, -4, -3, -2, -1, 0, 1, 4, 6, 8, 9, 10, 42, 100, 1000 };

            foreach (BigInteger nonPrime in nonPrimes)
            {
                Assert.False(Operations.IsPrime(nonPrime));
            }
        }

        [Fact]
        public void Operations_GetNextPrime_WithPrimeNumber_ReturnsNextPrime()
        {
            var primes = new BigInteger[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997 };

            for (int i = 0; i < primes.Length - 1; i++)
            {
                Assert.Equal(primes[i + 1], Operations.GetNextPrime(primes[i]));
            }
        }

        [Fact]
        public void Operations_GetNextPrime_WithNonPrimeNumber_ThrowsException()
        {
            var nonPrimes = new BigInteger[] { -5, -4, -3, -2, -1, 0, 1, 4, 6, 8, 9, 10, 42, 100, 1000 };

            foreach (BigInteger nonPrime in nonPrimes)
            {
                Assert.Throws<ArgumentException>(() => Operations.GetNextPrime(nonPrime));
            }
        }

        [Theory]
        [InlineData(-1, 7, 3)]
        [InlineData(4, 5, 8)]
        [InlineData(5, 2, 1)]
        [InlineData(6, 1, 5)]
        [InlineData(4, 9, 5)]
        [InlineData(-4, 7, 4)]
        [InlineData(1, 1, 5)]
        [InlineData(8, 2, 9)]
        [InlineData(2, 3, 6)]
        [InlineData(2, -3, 1)]
        [InlineData(-1, 6, 1)]
        [InlineData(9, 6, 6)]
        [InlineData(8, 4, 3)]
        [InlineData(5, 4, 1)]
        [InlineData(-9, 7, -2)]
        [InlineData(3, 2, 9)]
        [InlineData(9, 9, 3)]
        [InlineData(7, 8, 1)]
        [InlineData(8, -8, 5)]
        [InlineData(5, 6, 4)]
        [InlineData(-9, -8, 1)]
        [InlineData(5, 8, 8)]
        [InlineData(5, 8, 4)]
        [InlineData(7, 2, 2)]
        [InlineData(2, 5, 1)]
        [InlineData(2, 3, 3)]
        [InlineData(9, 8, 2)]
        [InlineData(-2, -8, -1)]
        public void Operations_IsPythagoreanTriplet_WithNonPythagoreanTriplet_ReturnsFalse(int a, int b, int c)
        {
            Assert.False(Operations.IsPythagoreanTriplet(a, b, c));
        }

        [Theory]
        [InlineData(3, 4, 5)]
        [InlineData(5, 12, 13)]
        [InlineData(8, 15, 17)]
        [InlineData(7, 24, 25)]
        [InlineData(20, 21, 29)]
        [InlineData(12, 35, 37)]
        [InlineData(9, 40, 41)]
        [InlineData(28, 45, 53)]
        [InlineData(11, 60, 61)]
        [InlineData(16, 63, 65)]
        [InlineData(33, 56, 65)]
        [InlineData(48, 55, 73)]
        [InlineData(13, 84, 85)]
        [InlineData(36, 77, 85)]
        [InlineData(39, 80, 89)]
        [InlineData(65, 72, 97)]
        [InlineData(-28, 45, 53)]
        [InlineData(-60, -11, 61)]
        public void Operations_IsPythagoreanTriplet_WithPythagoreanTriplet_ReturnsTrue(int a, int b, int c)
        {
            Assert.True(Operations.IsPythagoreanTriplet(a, b, c));
        }

        [Fact]
        public void Operations_Factorial_WithStrictlyNegativeInteger_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.Factorial(-42));
        }

        [Theory]
        [InlineData(0, "1")]
        [InlineData(1, "1")]
        [InlineData(2, "2")]
        [InlineData(3, "6")]
        [InlineData(4, "24")]
        [InlineData(5, "120")]
        [InlineData(6, "720")]
        [InlineData(42, "1405006117752879898543142606244511569936384000000000")]
        [InlineData(100, "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000")]
        public void Operations_Factorial_WithPositiveInteger_ThrowsException(int n, string expected)
        {
            Assert.Equal(BigInteger.Parse(expected), Operations.Factorial(n));
        }

        [Fact]
        public void Operations_BinomialCoefficient_WithNegativeArguments_ReturnsZero()
        {
            Assert.Equal(0, Operations.BinomialCoefficient(-1, 10));
            Assert.Equal(0, Operations.BinomialCoefficient(10, -1));
        }

        [Fact]
        public void Operations_BinomialCoefficient_WithKGreaterThanN_ReturnsZero()
        {
            Assert.Equal(0, Operations.BinomialCoefficient(10, 12));
        }

        [Theory]
        [InlineData(0, 0, 1)]
        [InlineData(9, 5, 126)]
        [InlineData(10, 8, 45)]
        [InlineData(5, 1, 5)]
        [InlineData(12, 10, 66)]
        public void Operations_BinomialCoefficient_WithNGreaterThanOrEqualToK_ReturnsZero(int n, int k, int expected)
        {
            Assert.Equal(expected, Operations.BinomialCoefficient(n, k));
        }

        [Fact]
        public void Operations_MultinomialCoefficient_WithStrictlyNegativeN_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.MultinomialCoefficient(-1, new[] { 1, 2, 3 }));
        }

        [Fact]
        public void Operations_MultinomialCoefficient_WithNullK_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Operations.MultinomialCoefficient(10, null));
        }

        [Fact]
        public void Operations_MultinomialCoefficient_WithEmptyK_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.MultinomialCoefficient(10, new int[0]));
        }

        [Theory]
        [InlineData(12, new[] { 1, 5, 3, 2, 1 }, 3684627)]
        [InlineData(14, new[] { 1, 0, 1, 9, 3 }, 240234)]
        [InlineData(54, new[] { 54 }, 1)]
        public void Operations_MultinomialCoefficient_WithNotEmptyK_ReturnsMultinomialCoefficient(int n, int[] k, int expected)
        {
            Assert.Equal(expected, Operations.MultinomialCoefficient(n, k));
        }

        [Fact]
        public void Operations_CountDivisors_WithStrictlyNegativeArgument_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.CountDivisors(-1));
        }

        [Fact]
        public void Operations_CountDivisors_WithPositiveArgument_ReturnsCount()
        {
            var expected = new[] { 0, 1, 2, 2, 3, 2, 4, 2, 4, 3, 4, 2, 6, 2, 4, 4, 5 };

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.Equal(expected[i], Operations.CountDivisors(i));
            }
        }

        [Fact]
        public void Operations_GreatestCommonDivisor_WithNoArguments_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.GreatestCommonDivisor());
        }

        [Fact]
        public void Operations_GreatestCommonDivisor_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Operations.GreatestCommonDivisor(null));
        }

        [Theory]
        [InlineData(new[] { 0, 0 }, 0)]
        [InlineData(new[] { 42 }, 42)]
        [InlineData(new[] { 12, 0 }, 12)]
        [InlineData(new[] { 42, 56 }, 14)]
        [InlineData(new[] { -42, 56 }, 14)]
        [InlineData(new[] { 42, -56 }, 14)]
        [InlineData(new[] { -42, -56 }, 14)]
        [InlineData(new[] { 461952, 116298 }, 18)]
        [InlineData(new[] { 24826148, 45296490 }, 526)]
        [InlineData(new[] { 30, 40, 60 }, 10)]
        [InlineData(new[] { 12, 24, 27, 30, 36 }, 3)]
        public void Operations_GreatestCommonDivisor_WithIntegers_ReturnsGreatestCommonDivisor(int[] nums, int expected)
        {
            Assert.Equal(expected, Operations.GreatestCommonDivisor(nums));
        }

        [Fact]
        public void Operations_LeastCommonMultiple_WithNullArgument_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Operations.LeastCommonMultiple(null));
        }

        [Fact]
        public void Operations_LeastCommonMultiple_WithNoArguments_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.LeastCommonMultiple());
        }

        [Fact]
        public void Operations_LeastCommonMultiple_WithOneArgument_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => Operations.LeastCommonMultiple(42));
        }

        [Theory]
        [InlineData(new[] { 74, 28 }, 1036)]
        [InlineData(new[] { 2, 40, 100, 97, 23 }, 446200)]
        [InlineData(new[] { 88, 43, 24, 50, 94 }, 13338600)]
        [InlineData(new[] { 96, 81, 81, 34, 83, 7, 96, 27 }, 25601184)]
        public void Operations_LeastCommonMultiple_WithMultipleArguments_ReturnsLeastCommonMultiple(int[] nums, int expected)
        {
            Assert.Equal(expected, Operations.LeastCommonMultiple(nums.Select(n => new BigInteger(n)).ToArray()));
        }
    }
}
