using System;
using System.Collections.Generic;

namespace Supernova.Foundation.Core.Trees
{
    /// <summary>
    /// Generic utils for data trees.
    /// </summary>
    public static class TreeUtils
    {
        /// <summary>
        /// Returns a hash set containing each of the nodes of a tree. Depth first.
        /// </summary>
        /// <typeparam name="T">The type of the nodes.</typeparam>
        /// <param name="root">The root of the tree.</param>
        /// <param name="nodesSelector">A function to select children nodes.</param>
        /// <returns>The nodes hash set.</returns>
        public static HashSet<T> Flatten<T>(T root, Func<T, IEnumerable<T>> nodesSelector)
        {
            Guard.NotNull(root, nameof(root));
            Guard.NotNull(nodesSelector, nameof(nodesSelector));

            var set = new HashSet<T> { root };

            Visit(root);

            void Visit(T node)
            {
                if (!set.Contains(node)) set.Add(node);

                foreach (T child in nodesSelector(node))
                {
                    if (!set.Contains(child)) Visit(child);
                }
            }

            return set;
        }
    }
}
