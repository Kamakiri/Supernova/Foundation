using System;
using System.Collections.Generic;

namespace Supernova.Foundation.Core.Extensions
{
    /// <summary>
    /// Provides extension methods for dictionaries.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Adds a key value pair to a dictionary. If the dictionary already contains the key, the value is updated.
        /// </summary>
        /// <typeparam name="TKey">The key type.</typeparam>
        /// <typeparam name="TValue">The value type.</typeparam>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns><c>true</c> if the value was added, <c>false</c> if it was updated.</returns>
        public static bool AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            Guard.NotNull(dictionary, nameof(dictionary));
            Guard.NotNull(key, nameof(key));

            if (dictionary.ContainsKey(key))
            {
                dictionary[key] = value;
                return false;
            }

            dictionary.Add(key, value);
            return true;
        }

        /// <summary>
        /// Merges 2 dictionaries.
        /// </summary>
        /// <typeparam name="TKey">The key type.</typeparam>
        /// <typeparam name="TValue">The key value.</typeparam>
        /// <param name="dic1">The first dictionary.</param>
        /// <param name="dic2">The second dictionary.</param>
        /// <param name="resolution">A function to solve merge conflicts.</param>
        /// <returns>The merge result.</returns>
        public static Dictionary<TKey, TValue> Merge<TKey, TValue>(this Dictionary<TKey, TValue> dic1, Dictionary<TKey, TValue> dic2, Func<TValue, TValue, TValue> resolution)
        {
            Guard.NotNull(dic1, nameof(dic1));
            Guard.NotNull(dic2, nameof(dic2));
            Guard.NotNull(resolution, nameof(resolution));

            var merge = new Dictionary<TKey, TValue>(dic1);

            foreach (KeyValuePair<TKey, TValue> pair in dic2)
            {
                if (merge.ContainsKey(pair.Key)) merge[pair.Key] = resolution(merge[pair.Key], pair.Value);
                else merge.Add(pair.Key, pair.Value);
            }

            return merge;
        }
    }
}
