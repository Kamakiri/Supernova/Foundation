using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Supernova.Foundation.Core.Extensions
{
    /// <summary>
    /// Provides extension methods for enumerables.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Returns the first integer (by increasing value) that is not present in the enumerable.
        /// For example, if the input is { 0, 1, 2, 4 } the output will be 3.
        /// </summary>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="start">The value considered as the lower integer. For example if its 0 then { 1, 2, 4 } returns 0, if its 1 then { 1, 2, 4 } retrurns 3.</param>
        /// <returns>The result.</returns>
        public static int FirstNotIn(this IEnumerable<int> enumerable, int start = 0)
        {
            Guard.NotNull(enumerable, nameof(enumerable));

            var list = enumerable.OrderBy(x => x).ToList();

            if (list.Count == 0 || start < list[0] || start > list.Last()) return start;
            if (list.Count == 1) return list[0] == start ? start + 1 : start;

            for (int i = 0; i < list.Count - 1; i++)
            {
                int x = list[i];
                int y = list[i + 1];

                if (x + 1 >= start && x + 1 < y) return x + 1;
            }

            return list.Last() + 1;
        }

        /// <summary>
        /// Picks a random element from the list.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The element.</returns>
        public static T PickRandom<T>(this IList<T> list)
        {
            Guard.NotNullOrEmpty(list, nameof(list));
            return list[Randomizer.Next(0, list.Count)];
        }

        /// <summary>
        /// Gets an element at the specified index and removes it from the list.
        /// </summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="index">The index.</param>
        /// <returns>The element.</returns>
        public static T GetAndRemoveAt<T>(this IList<T> list, int index)
        {
            Guard.NotNull(list, nameof(list));

            T item = list[index];
            list.RemoveAt(index);
            return item;
        }

        /// <summary>
        /// Checks if a collection has at least the specified number of elements.
        /// </summary>
        /// <typeparam name="T">The elements type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="count">The count.</param>
        /// <returns>The check result.</returns>
        public static bool HasAtLeast<T>(this ICollection<T> collection, int count)
        {
            Guard.NotNull(collection, nameof(collection));
            return collection.Count >= count;
        }

        /// <summary>
        /// Checks if an enumerable has at least the specified number of elements.
        /// </summary>
        /// <typeparam name="T">The elements type.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="count">The count.</param>
        /// <returns>The check result.</returns>
        public static bool HasAtLeast<T>(this IEnumerable<T> enumerable, int count)
        {
            Guard.NotNull(enumerable, nameof(enumerable));
            return count <= 0 || enumerable.Skip(count - 1).Any();
        }

        /// <summary>
        /// Applies an action to all items of an enumerable with deferred execution and returns the items.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="action">The action.</param>
        /// <returns>The items after the action is applied.</returns>
        public static IEnumerable<T> Apply<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            Guard.NotNull(enumerable, nameof(enumerable));
            Guard.NotNull(action, nameof(action));

            foreach (T item in enumerable)
            {
                action(item);
                yield return item;
            }
        }

        /// <summary>
        /// Blend together two enumerables, alternating items from each one by one.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="enumerable1">The first enumerable.</param>
        /// <param name="enumerable2">The second enumerable.</param>
        /// <returns>The interlaced enumerable.</returns>
        public static IEnumerable<T> Interlace<T>(this IEnumerable<T> enumerable1, IEnumerable<T> enumerable2)
        {
            Guard.NotNull(enumerable1, nameof(enumerable1));
            Guard.NotNull(enumerable2, nameof(enumerable2));

            using (IEnumerator<T> enumerator1 = enumerable1.GetEnumerator())
            using (IEnumerator<T> enumerator2 = enumerable2.GetEnumerator())
            {
                while (true)
                {
                    bool move1 = enumerator1.MoveNext();
                    bool move2 = enumerator2.MoveNext();

                    if (move1) yield return enumerator1.Current;
                    if (move2) yield return enumerator2.Current;
                    if (!move1 && !move2) break;
                }
            }
        }

        /// <summary>
        /// Swaps items at position <paramref name="i"/> and <paramref name="j"/> in a list.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="i">The position of the first item.</param>
        /// <param name="j">The position of the second item.</param>
        public static void Swap<T>(this IList<T> list, int i, int j)
        {
            Guard.NotNull(list, nameof(list));

            if (i == j) return;

            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }

        /// <summary>
        /// Shuffles a list using the Fisher–Yates algorithm.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="list">The list.</param>
        public static void Shuffle<T>(this IList<T> list) => Shuffler.FisherYates(list);

        /// <summary>
        /// Slices an array.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="array">The array.</param>
        /// <param name="index">The starting index.</param>
        /// <param name="count">The number of items.</param>
        /// <returns>The slice.</returns>
        public static T[] Slice<T>(this T[] array, int index, int count)
        {
            Guard.NotNull(array, nameof(array));
            Guard.InRange(0, array.Length, index, nameof(index));
            Guard.InRange(0, array.Length, count, nameof(count));

            T[] slice = new T[count];
            Array.Copy(array, index, slice, 0, count);
            return slice;
        }

        /// <summary>
        /// Splits an array in two arrays.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="array">The original array.</param>
        /// <param name="index">The index at which to split (the element at this index will be the first item of the second array).</param>
        /// <returns>The two resulting arrays.</returns>
        public static (T[], T[]) Split<T>(this T[] array, int index)
        {
            Guard.NotNull(array, nameof(array));
            Guard.InRange(0, array.Length, index, nameof(index));

            int rest = array.Length - index;

            var first = new T[index];
            var last = new T[rest];

            Array.Copy(array, 0, first, 0, index);
            Array.Copy(array, index, last, 0, rest);

            return (first, last);
        }

        /// <summary>
        /// Gets whether the enumerable is empty or not.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="argument">The enumerable.</param>
        /// <returns><c>true</c> if the enumerable is empty, <c>false</c> if not.</returns>
        public static bool IsEmpty<T>(this IEnumerable<T> argument)
        {
            Guard.NotNull(argument, nameof(argument));

            switch (argument)
            {
                case ICollection<T> collection:
                    return collection.Count == 0;

                case ICollection collection:
                    return collection.Count == 0;
            }

            return !argument.Any();
        }

        /// <summary>
        /// Checks whether an enumerable contains duplicates or not.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns><c>true</c> if enumerable contains duplicates, <c>false</c> otherwise.</returns>
        public static bool HasDuplicates<T>(this IEnumerable<T> enumerable) => enumerable.GroupBy(x => x).Any(g => g.Count() > 1);
    }
}
