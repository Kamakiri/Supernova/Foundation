using System.Globalization;
using System.Linq;
using System.Text;

namespace Supernova.Foundation.Core.Extensions
{
    /// <summary>
    /// Provides extension methods for strings.
    /// </summary>
    public static class StringExtensions
    {
        private static char[] _separators = new[] { '-', '�', '�', '\\', '/', '|' };

        /// <summary>
        /// Repeats a string.
        /// If the <paramref name="str"/> is null or the <paramref name="times"/> is zero, returns an empty string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="times">The number of times to repeat the string.</param>
        /// <returns>The repeated string.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws if <paramref name="times"/> is lower than zero.</exception>
        public static string Repeat(this string str, int times) => string.Concat(Enumerable.Repeat(str, times));

        /// <summary>
        /// Changes a string to pascal case (FooBar), using whitespace, punctuations and '-', '�', '�', '\\', '/' and '|' as delimiters.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="culture">An optional culture. If not specified, use invariant culture.</param>
        /// <returns>The pascal case string.</returns>
        public static string ToPascalCase(this string str, CultureInfo culture = null) => str.ToCase(culture, true);

        /// <summary>
        /// Changes a string to camel case (fooBar), using whitespace, punctuations and '-', '�', '�', '\\', '/' and '|' as delimiters.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="culture">An optional culture. If not specified, use invariant culture.</param>
        /// <returns>The camel case string.</returns>
        public static string ToCamelCase(this string str, CultureInfo culture = null) => str.ToCase(culture);

        /// <summary>
        /// Changes a string to snake case (foo_bar), using whitespace, punctuations and '-', '�', '�', '\\', '/' and '|' as delimiters.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="culture">An optional culture. If not specified, use invariant culture.</param>
        /// <returns>The camel case string.</returns>
        public static string ToSnakeCase(this string str, CultureInfo culture = null) => str.ToCase(culture, false, false, "_");

        /// <summary>
        /// Changes a string to kebab case (foo-bar), using whitespace, punctuations and '-', '�', '�', '\\', '/' and '|' as delimiters.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <param name="culture">An optional culture. If not specified, use invariant culture.</param>
        /// <returns>The camel case string.</returns>
        public static string ToKebabCase(this string str, CultureInfo culture = null) => str.ToCase(culture, false, false, "-");

        private static string ToCase(this string str, CultureInfo culture, bool firstCharacterUpper = false, bool upperNewWords = true, string separator = "")
        {
            Guard.NotNull(str, nameof(str));

            str = str.Trim();
            culture = culture ?? CultureInfo.InvariantCulture;
            bool newWord = firstCharacterUpper;
            var builder = new StringBuilder();

            foreach (char c in str)
            {
                if (char.IsWhiteSpace(c) || char.IsPunctuation(c) || _separators.Contains(c))
                {
                    newWord = true;
                    continue;
                }

                if (newWord)
                {
                    builder.Append(separator);
                    builder.Append(upperNewWords ? char.ToUpper(c, culture) : char.ToLower(c, culture));
                    newWord = false;
                }
                else builder.Append(char.ToLower(c, culture));
            }

            return builder.ToString();
        }
    }
}
