﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Supernova.Foundation.Core.Exceptions;

namespace Supernova.Foundation.Core
{
    /// <summary>
    /// Provides precondition checks.
    /// </summary>
    public static class Guard
    {
        /// <summary>
        /// Throws an <see cref="DirectoryNotFoundException"/> if the directory corresponding to the provided path is not found.
        /// </summary>
        /// <param name="path">The path to the directory.</param>
        public static void DirectoryExist(string path)
        {
            NotNullOrWhitespace(path, nameof(path));
            if (!Directory.Exists(path)) throw new DirectoryNotFoundException($"The specified directory was not found. ({path})");
        }

        /// <summary>
        /// Throws an <see cref="FileNotFoundException"/> if the file corresponding to the provided path is not found.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        public static void FileExist(string path)
        {
            NotNullOrWhitespace(path, nameof(path));
            if (!File.Exists(path)) throw new FileNotFoundException("The specified file was not found.", path);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is strictly lower than the specified value.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="value">The comparison value.</param>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void GreaterThanOrEqualTo<T>(T value, T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(value) < 0) throw new ArgumentException($"Argument {argumentName} must be greater than or equal to {value}.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentOutOfRangeException"/> if the provided argument is not in the specified inclusive range.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="inclusiveLowerBound">The inclusive lower bound.</param>
        /// <param name="inclusiveUpperBound">The inclusive upper bound.</param>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void InRange<T>(T inclusiveLowerBound, T inclusiveUpperBound, T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            NotNull(inclusiveLowerBound, nameof(inclusiveLowerBound));
            NotNull(inclusiveUpperBound, nameof(inclusiveUpperBound));

            if (argument.CompareTo(inclusiveLowerBound) < 0 || argument.CompareTo(inclusiveUpperBound) > 0)
            {
                throw new ArgumentOutOfRangeException($"Argument {argumentName} must be in range [{inclusiveLowerBound}:{inclusiveUpperBound}] (actual {argument}).", argumentName);
            }
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is strictly greater than the specified value.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="value">The comparison value.</param>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void LowerThanOrEqualTo<T>(T value, T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(value) > 0) throw new ArgumentException($"Argument {argumentName} must be lower than or equal to {value}.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument doesn't match the provided regular expression.
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        /// <param name="regex">The regular expression.</param>
        /// <param name="options">The regular expression options.</param>
        public static void Match(string argument, string argumentName, string regex, RegexOptions options = RegexOptions.None)
        {
            NotNullOrWhitespace(argument, nameof(argument));
            NotNullOrWhitespace(argumentName, nameof(argumentName));
            NotNullOrWhitespace(regex, nameof(regex));

            if (!Regex.IsMatch(argument, regex, options)) throw new ArgumentException($"Argument {argumentName} must match the regex.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument doesn't contain at least the specified number of items.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="count">The minimum count.</param>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void MinimumCount<T>(int count, ICollection<T> argument, string argumentName)
        {
            NotNull(argument, argumentName);
            if (argument.Count < count) throw new ArgumentException($"Argument {argumentName} must have at least {count} items (was {argument.Count}).", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is not negative.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void Negative<T>(T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(default(T)) > 0) throw new ArgumentException($"Argument {argumentName} must be negative.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentNullException"/> if the provided argument is null.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void NotNull<T>(T argument, string argumentName)
        {
            if (argument == null) throw new ArgumentNullException(argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is null or empty.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void NotNullOrEmpty<T>(IEnumerable<T> argument, string argumentName)
        {
            NotNull(argument, argumentName);
            if (!argument.Any()) throw new ArgumentException($"Enumerable {argumentName} must not be empty.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is null or whitespace.
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void NotNullOrWhitespace(string argument, string argumentName)
        {
            NotNull(argument, argumentName);
            if (string.IsNullOrWhiteSpace(argument)) throw new ArgumentException($"String {argumentName} must not be white space.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is not positive.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void Positive<T>(T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(default(T)) < 0) throw new ArgumentException($"Argument {argumentName} must be positive.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided condition is not met (i.e. doesn't evaluates to true).
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <param name="description">The condition description.</param>
        public static void Require(Func<bool> condition, string description)
        {
            NotNull(condition, nameof(condition));
            if (!condition()) throw new ArgumentException($"Condition \"{description}\" must be true.");
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided condition is not met (i.e. isn't true).
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <param name="description">The condition description.</param>
        public static void Require(bool condition, string description)
        {
            if (!condition) throw new ArgumentException($"Condition \"{description}\" must be true.");
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is lower than or equal to the specified value.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="value">The comparison value.</param>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void StrictlyGreaterThan<T>(T value, T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(value) <= 0) throw new ArgumentException($"Argument {argumentName} must be strictly greater than {value}.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is greater than or equal to the specified value.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="value">The comparison value.</param>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void StrictlyLowerThan<T>(T value, T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(value) >= 0) throw new ArgumentException($"Argument {argumentName} must be strictly lower than {value}.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is not strictly negative.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void StrictlyNegative<T>(T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(default(T)) >= 0) throw new ArgumentException($"Argument {argumentName} must be strictly negative.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentException"/> if the provided argument is not strictly positive.
        /// </summary>
        /// <typeparam name="T">The argument type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void StrictlyPositive<T>(T argument, string argumentName) where T : IComparable
        {
            NotNull(argument, argumentName);
            if (argument.CompareTo(default(T)) <= 0) throw new ArgumentException($"Argument {argumentName} must be strictly positive.", argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentTypeException"/> if the provided argument is not of type <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The expected type.</typeparam>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">The argument name.</param>
        public static void Type<T>(object argument, string argumentName)
        {
            NotNull(argument, argumentName);
            if (!(argument is T)) throw new ArgumentTypeException(argumentName, typeof(T), argument.GetType());
        }
    }
}
