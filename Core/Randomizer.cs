﻿using System;
using System.Threading;
using Supernova.Foundation.Core.Extensions;

namespace Supernova.Foundation.Core
{
    /// <summary>
    /// A thread-safe randomizer.
    /// </summary>
    public static class Randomizer
    {
        private static readonly ThreadLocal<Random> _rnd = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref _globalSeed)));

        private static int _globalSeed = Environment.TickCount;

        /// <summary>
        /// Returns a non-negative random integer.
        /// </summary>
        /// <returns>The result.</returns>
        public static int Next() => _rnd.Value.Next();

        /// <summary>
        /// Returns a non-negative random integer that is greater than or equal to 0 and less than the specified maximum.
        /// </summary>
        /// <param name="maxValue">The exclusive upper bound of the random number to be generated. maxValue must be greater than or equal to 0.</param>
        /// <returns>The result.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws whan maxValue is less than 0.</exception>
        public static int Next(int maxValue) => _rnd.Value.Next(maxValue);

        /// <summary>
        /// Returns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue">The inclusive lower bound of the random number returned.</param>
        /// <param name="maxValue">The exclusive upper bound of the random number returned. <paramref name="maxValue"/> must be greater than or equal to <paramref name="minValue"/>.</param>
        /// <returns>The result.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Throws when <paramref name="minValue"/> in greater than <paramref name="maxValue"/>.</exception>
        public static int Next(int minValue, int maxValue) => _rnd.Value.Next(minValue, maxValue);

        /// <summary>
        /// Fills the elements of a specified array of bytes with random numbers.
        /// </summary>
        /// <param name="buffer">An array of bytes to contain random numbers.</param>
        /// <exception cref="System.ArgumentNullException">Throws when <paramref name="buffer"/> is null.</exception>
        public static void NextBytes(byte[] buffer) => _rnd.Value.NextBytes(buffer);

        /// <summary>
        /// Returns a random floating-point number that is greater than or equal to 0.0, and less than 1.0.
        /// </summary>
        /// <returns>A double-precision floating point number that is greater than or equal to 0.0, and less than 1.0.</returns>
        public static double NextDouble() => _rnd.Value.NextDouble();

        /// <summary>
        /// Returns a random bool.
        /// </summary>
        /// <returns>The result.</returns>
        public static bool NextBool() => new[] { true, false }.PickRandom();
    }
}
