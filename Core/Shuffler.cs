﻿using System.Collections.Generic;
using Supernova.Foundation.Core.Extensions;

namespace Supernova.Foundation.Core
{
    /// <summary>
    /// Provides shuffling methods.
    /// </summary>
    public static class Shuffler
    {
        /// <summary>
        /// Shuffles the input list using the Fisher-Yates algorithm.
        /// </summary>
        /// <typeparam name="T">The items type.</typeparam>
        /// <param name="list">The list to shuffle.</param>
        public static void FisherYates<T>(IList<T> list)
        {
            Guard.NotNull(list, nameof(list));

            if (list.Count == 1) return;

            for (int i = 0; i < list.Count - 1; i++)
            {
                int j = Randomizer.Next(i, list.Count);
                list.Swap(i, j);
            }
        }
    }
}
