﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Supernova.Foundation.Core.Extensions;

namespace Supernova.Foundation.Core.IO
{
    /// <summary>
    /// Provides helper methods to extend <see cref="File"/>.
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Copies a file to a new location. If a file with the same name already exists, increments its name.
        /// For example, "foo.bar" will become "foo (3).bar" if "foo.bar" and "foo (2).bar" already exist.
        /// </summary>
        /// <param name="sourceFilePath">The source file path.</param>
        /// <param name="targetFilePath">The target file path.</param>
        public static void CopyIncrement(string sourceFilePath, string targetFilePath)
        {
            Guard.FileExist(sourceFilePath);
            Guard.NotNullOrWhitespace(targetFilePath, nameof(targetFilePath));

            File.Copy(sourceFilePath, GetIncrementedPath(targetFilePath));
        }

        /// <summary>
        /// Write the contents to file. If the file already exist, a new file with incremented name is created.
        /// For example, "foo.bar" will become "foo (3).bar" if "foo.bar" and "foo (2).bar" already exist.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="content">The content.</param>
        public static void WriteAllTextIncrement(string path, string content) => File.WriteAllText(GetIncrementedPath(path), content);

        /// <summary>
        /// Write the contents to file. If the file already exist, a new file with incremented name is created.
        /// For example, "foo.bar" will become "foo (3).bar" if "foo.bar" and "foo (2).bar" already exist.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="content">The content.</param>
        /// <param name="encoding">The content encoding.</param>
        public static void WriteAllTextIncrement(string path, string content, Encoding encoding) => File.WriteAllText(GetIncrementedPath(path), content, encoding);

        /// <summary>
        /// Gets the path of a file, incremented if a file with the same path already exist.
        /// For example, "foo.bar" will become "foo (3).bar" if "foo.bar" and "foo (2).bar" already exist.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetIncrementedPath(string filePath)
        {
            Guard.NotNullOrWhitespace(filePath, nameof(filePath));

            if (!File.Exists(filePath)) return filePath;

            string directory = Path.GetDirectoryName(filePath);
            string name = Path.GetFileNameWithoutExtension(filePath);
            string extension = Path.GetExtension(filePath);

            string[] files = Directory.GetFiles(directory).Select(Path.GetFileName).ToArray();
            var regex = new Regex($@"^{Regex.Escape(name)}(?: \((?<count>\d+)\)){Regex.Escape(extension)}$");
            int number = files
                .Select(f => regex.Match(f))
                .Where(m => m.Success)
                .Select(m => m.Groups["count"].Value)
                .Select(int.Parse)
                .OrderBy(i => i)
                .FirstNotIn(2);

            return Path.Combine(directory, $"{name} ({number}){extension}");
        }

        /// <summary>
        /// Moves all directory content to its parent directory.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        public static void MoveToParentDirectory(string directoryPath)
        {
            Guard.DirectoryExist(directoryPath);

            string parent = Path.Combine(directoryPath, "..");

            foreach (FileInfo file in Directory.EnumerateFiles(directoryPath, "*", SearchOption.TopDirectoryOnly).Select(path => new FileInfo(path)))
            {
                file.MoveTo(Path.Combine(parent, file.Name));
            }

            foreach (DirectoryInfo directory in Directory.EnumerateDirectories(directoryPath, "*", SearchOption.TopDirectoryOnly).Select(path => new DirectoryInfo(path)))
            {
                directory.MoveTo(Path.Combine(parent, directory.Name));
            }

            Directory.Delete(directoryPath);
        }
    }
}
