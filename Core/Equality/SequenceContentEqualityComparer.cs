using System.Collections.Generic;
using System.Linq;

namespace Supernova.Foundation.Core.Equality
{
    /// <summary>
    /// Provides equality comparison for enumerables, based on the sequence equality.
    /// </summary>
    public class SequenceContentEqualityComparer<T> : IEqualityComparer<IEnumerable<T>>
    {
        private readonly bool _ignoreOrder;

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceContentEqualityComparer{T}"/> class.
        /// </summary>
        /// <param name="ignoreOrder">If <c>true</c>, ignore the order of the elements in the enumerables.</param>
        public SequenceContentEqualityComparer(bool ignoreOrder = false)
        {
            _ignoreOrder = ignoreOrder;
        }

        /// <summary>
        /// Checks if the providen enumerables are equal, by reference or by sequence equality.
        /// </summary>
        /// <param name="x">The first expression.</param>
        /// <param name="y">The second expression.</param>
        /// <returns><c>true</c> if <paramref name="x"/> and <paramref name="y"/> are equal, <c>false</c> otherwise.</returns>
        public bool Equals(IEnumerable<T> x, IEnumerable<T> y)
        {
            if (x is null && y is null) return true;
            if (x is null || y is null) return false;
            return x.Equals(y) || _ignoreOrder ? !(x.Except(y).Any() || y.Except(x).Any()) : x.SequenceEqual(y);
        }

        /// <summary>
        /// Computes the hash code of the provided enumerable, based on its sequence.
        /// </summary>
        /// <param name="obj">The enumerable.</param>
        /// <returns>The hash code.</returns>
        public int GetHashCode(IEnumerable<T> obj)
        {
            if (obj == null) return 0;

            unchecked
            {
                int hash = 17;
                foreach (T element in obj)
                {
                    hash = hash * 31 + element.GetHashCode();
                }
                return hash;
            }
        }
    }
}
