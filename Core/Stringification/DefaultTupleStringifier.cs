﻿namespace Supernova.Foundation.Core.Stringification
{
    public class DefaultTupleStringifier<T1, T2> : IStringifier<(T1, T2)>
    {
        public string Stringify((T1, T2) input) => $"({input.Item1}, {input.Item2})";
    }
}
