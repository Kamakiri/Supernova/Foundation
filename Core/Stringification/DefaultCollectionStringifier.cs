﻿using System.Collections.Generic;

namespace Supernova.Foundation.Core.Stringification
{
    public class DefaultCollectionStringifier<T> : IStringifier<ICollection<T>>
    {
        public string Stringify(ICollection<T> input)
            => input == null || input.Count == 0
                ? ""
                : $"[{string.Join(", ", input)}]";
    }
}
