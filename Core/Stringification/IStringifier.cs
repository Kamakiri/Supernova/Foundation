﻿namespace Supernova.Foundation.Core.Stringification
{
    public interface IStringifier<T>
    {
        string Stringify(T input);
    }
}
