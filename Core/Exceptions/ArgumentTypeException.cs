﻿using System;

namespace Supernova.Foundation.Core.Exceptions
{
    /// <summary>
    /// An exception to throw when one of the arguments provided to a method is not of the expected type.
    /// </summary>
    public class ArgumentTypeException : ArgumentException
    {
        /// <summary>
        /// Initializes an new instance of the <see cref="ArgumentTypeException"/> exception.
        /// </summary>
        /// <param name="argumentName">The argument name.</param>
        /// <param name="expectedType">The expected type.</param>
        /// <param name="actualType">The actual type.</param>
        public ArgumentTypeException(string argumentName, Type expectedType, Type actualType) : base($"Expected type {expectedType.FullName} but was {actualType.FullName}.", argumentName)
        {
            ExpectedType = expectedType;
            ActualType = actualType;
        }

        /// <summary>
        /// Gets the expected type.
        /// </summary>
        public Type ExpectedType { get; }

        /// <summary>
        /// Gets the actual type.
        /// </summary>
        public Type ActualType { get; }
    }
}
