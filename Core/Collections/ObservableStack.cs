﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Supernova.Foundation.Core.Collections
{
    public class ObservableStack<T> : Stack<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes an new instance of the <see cref="ObservableStack{T}"/> class.
        /// </summary>
        public ObservableStack() : base()
        {
        }

        /// <summary>
        /// Initializes an new instance of the <see cref="ObservableStack{T}"/> class.
        /// </summary>
        /// <param name="collection">Initial data.</param>
        public ObservableStack(IEnumerable<T> collection) : base(collection)
        {
        }

        /// <summary>
        /// Initializes an new instance of the <see cref="ObservableStack{T}"/> class.
        /// </summary>
        /// <param name="capacity">Initial capacity.</param>
        public ObservableStack(int capacity) : base(capacity)
        {
        }

        /// <summary>
        /// The event called when collection changed.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// The event called when peoperty changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Removes all items from the stack.
        /// </summary>
        public new virtual void Clear()
        {
            base.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            OnPropertyChanged(nameof(Count));
        }

        /// <summary>
        /// Removes and returns the item at the top of the stack.
        /// </summary>
        /// <returns>The item.</returns>
        public new virtual T Pop()
        {
            T item = base.Pop();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
            OnPropertyChanged(nameof(Count));
            return item;
        }

        /// <summary>
        /// Inserts an item at the top of the stack.
        /// </summary>
        /// <param name="item">The item.</param>
        public new virtual void Push(T item)
        {
            base.Push(item);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            OnPropertyChanged(nameof(Count));
        }

        /// <summary>
        /// Callback when collection changed.
        /// </summary>
        /// <param name="e">The event args.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e) => CollectionChanged?.Invoke(this, e);

        /// <summary>
        /// Calback when property changed.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
