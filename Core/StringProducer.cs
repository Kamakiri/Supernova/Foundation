﻿using System;

namespace Supernova.Foundation.Core
{
    /// <summary>
    /// A helper to produce strings.
    /// </summary>
    public static class StringProducer
    {
        /// <summary>
        /// Produces a years range with the two given years (e.g. 2017–2019).
        /// If the first year is grater than the second, it's placed last in the output range.
        /// If the years are the same, a single year is returned.
        /// </summary>
        /// <param name="year1">The first year.</param>
        /// <param name="year2">The second year.</param>
        /// <returns>The range.</returns>
        public static string YearsRange(int year1, int year2)
            => year1 == year2
                ? $"{year1}"
                : $"{Math.Min(year1, year2)}–{Math.Max(year1, year2)}";
    }
}
